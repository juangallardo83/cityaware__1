export default {
  items: [
    {
      name: 'Todas las temáticas',
      url: '/dashboard/todos',
      icon: 'icon-speedometer',
    },
    {
      name: 'Rol',
      url: '/dashboard/rol',
      icon: 'icon-speedometer',
    },
    {
      name: 'Otros',
      url: '/dashboard/otros',
      icon: 'icon-speedometer',
    },
  ]
}
